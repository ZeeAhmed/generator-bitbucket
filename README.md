# generator-bitbucket

A generator for [Yeoman](http://yeoman.io).

[![NPM](https://nodei.co/npm/generator-bitbucket.png?downloads=true)](https://nodei.co/npm/generator-bitbucket/)

## Yeoman Generators

To install generator-bitbucket from npm, run:

```
$ npm install -g generator-bitbucket
```

Finally, initiate the generator:

```
$ yo bitbucket
```

## Test

Assign your Bitbucket Username and Password as envrionment variables.
```
$ export BITBUCKET_USERNAME=elegantcoder
$ export BITBUCKET_PASSWORD=somepassword
$ npm test
```

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)
